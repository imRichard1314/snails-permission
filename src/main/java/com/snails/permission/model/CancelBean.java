package com.snails.permission.model;

/**
 * 权限取消
 *
 * @author lawrence
 * @date 2019-05-05 16:10
 */
public class CancelBean {

    private int requestCode;

    public CancelBean(int requestCode) {
        this.requestCode = requestCode;
    }

    public int getRequestCode() {
        return requestCode;
    }

    public void setRequestCode(int requestCode) {
        this.requestCode = requestCode;
    }

}
