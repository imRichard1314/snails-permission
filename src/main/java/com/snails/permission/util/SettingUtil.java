package com.snails.permission.util;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.text.TextUtils;

import com.snails.permission.iface.ISetting;
import com.snails.permission.support.DefaultSetting;
import com.snails.permission.support.HuaWeiSetting;
import com.snails.permission.support.OPPOSetting;
import com.snails.permission.support.ViVoSetting;

/**
 * @author lawrence
 * @date 2019-05-05 16:43
 */
public class SettingUtil {

    private static final String MANUFACTURER_HUAWEI = "HUAWEI";         //华为
    private static final String MANUFACTURER_MEIZU = "MEIZU";           //魅族
    private static final String MANUFACTURER_XIAOMI = "XIAOMI";         //小米
    private static final String MANUFACTURER_SONY = "SONY";             //索尼
    private static final String MANUFACTURER_OPPO = "OPPO";
    private static final String MANUFACTURER_LG = "LG";
    private static final String MANUFACTURER_VIVO = "VIVO";
    private static final String MANUFACTURER_SAMSUNG = "SAMSUNG";       //三星
    private static final String MANUFACTURER_LETV = "LETV";             //乐视
    private static final String MANUFACTURER_ZTE = "ZTE";               //中兴
    private static final String MANUFACTURER_YULONG = "YULONG";         //酷派
    private static final String MANUFACTURER_LENOVO = "LENOVO";         //联想

    private static String s2Up(String s) {
        return TextUtils.isEmpty(s) ? s : s.toUpperCase();
    }

    /**
     * 跳设置界面
     */
    public static void go2Setting(Context ctx) {
        ISetting iSetting = null;
        try {
            switch (s2Up(Build.MANUFACTURER)) {
//                case MANUFACTURER_HUAWEI: iSetting = new HuaWeiSetting(ctx); break;
//                case MANUFACTURER_VIVO: iSetting = new ViVoSetting(ctx); break;
//                case MANUFACTURER_OPPO: iSetting = new OPPOSetting(ctx); break;
                default: iSetting = new DefaultSetting(ctx); break;
            }
            final Intent intent = iSetting.getSetting();
            if (intent != null) ctx.startActivity(intent);
        } catch (Exception e) {
            e.printStackTrace();
            // 打开失败，使用默认方式打开
            iSetting = new DefaultSetting(ctx);
            final Intent intent = iSetting.getSetting();
            if (intent != null) ctx.startActivity(intent);
        }
    }

}