package com.snails.permission.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 权限被拒绝
 * -- 弹出系统权限弹窗，用户没有给权限，并且选中不再提示，这种情况称为权限被拒绝
 *
 * @author lawrence
 * @date 2019-05-05 16:06
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface PermissionDenied {

}
